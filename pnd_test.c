/*****************************************************************************/
/*  Copyright (C) 2020 Siemens Aktiengesellschaft. All rights reserved.      */
/*****************************************************************************/
/*  This program is protected by German copyright law and international      */
/*  treaties. The use of this software including but not limited to its      */
/*  Source Code is subject to restrictions as agreed in the license          */
/*  agreement between you and Siemens.                                       */
/*  Copying or distribution is not allowed unless expressly permitted        */
/*  according to your license agreement with Siemens.                        */
/*****************************************************************************/
/*                                                                           */
/*  P r o j e c t         &P: PROFINET IO Runtime Software              :P&  */
/*                                                                           */
/*  P a c k a g e         &W: PROFINET IO Runtime Software              :W&  */
/*                                                                           */
/*  C o m p o n e n t     &C: PnDriver                                  :C&  */
/*                                                                           */
/*  F i l e               &F: pnd_test.c                                :F&  */
/*                                                                           */
/*  V e r s i o n         &V: BC_PNDRIVER_P02.02.00.00_00.02.00.13      :V&  */
/*                                                                           */
/*  D a t e  (YYYY-MM-DD) &D: 2020-06-05                                :D&  */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*  D e s c r i p t i o n :                                                  */
/*                                                                           */
/*  Test Application for PNDriver                                            */
/*                                                                           */
/*****************************************************************************/
#define _CRTDBG_MAP_ALLOC

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pniobase.h"
#include "pnd_iob_core.h"
#include "pnd_pntrc.h"
#include "pnd_iodu.h"
#include "pnd_helper_functions.h"
#include "pndriver_product_info.h"


#ifdef PLF_PNDRIVER_WIN
#include <crtdbg.h>
#include <windows.h>        //used for locking and error handling
#endif

static PNIO_DEBUG_SETTINGS_TYPE debSet;
extern PNIO_UINT32 g_ApplHandle;

char *file_path          = "PNDriverBase_TestApp/Station_1.PN Driver_1.PNDriverConfiguration.xml";
char *file_name          = "Station_1.PN Driver_1.PNDriverConfiguration.xml";
char *rema_file_name     = "rema.xml";
char *preSelectedCp      = "";
char *product_file_path  = "";

    
void HR(int iLen) 
{
    int i;
    for (i = 0; i < iLen; i++)
        printf("-");

    printf("\n");
    return;
} 

void process_start_args(char *argv[], PNIO_BOOL* p_infoFromTxt)
{
	lockMutex();
    while (*(++argv))
    {
        if (**argv == '-' && strlen(*argv) == 2 && *(argv + 1) != NULL)
        {
            switch (*(*argv + 1))
            {
                break;
                case 'p':
                case 'P':
                {
                    *p_infoFromTxt = PNIO_TRUE;
                    product_file_path = malloc(sizeof(char*)*strlen(*(++argv)));
                    strcpy(product_file_path, *(argv));
                    if (product_file_path && ((*(product_file_path + strlen(product_file_path) - 1) == '\\')
                        || (*(product_file_path + strlen(product_file_path) - 1) == '/')))
                    {
                        *(product_file_path + strlen(product_file_path) - 1) = 0;  //strip the last char
                    }
                }
                break;
                case 'f':
                case 'F':
                    {
	                    file_path = malloc(sizeof(char*)*strlen(*(++argv)));
	                    strcpy(file_path, *(argv));
	                    if (file_path && ((*(file_path + strlen(file_path) - 1) == '\\')
	                        || (*(file_path + strlen(file_path) - 1) == '/')))
	                    {
	                        *(file_path + strlen(file_path) - 1) = 0;  //strip the last char
	                    }
                    }
                break;
                case 'd':
                case 'D':
                    {
	                    int dir_list_size = 0;
	                    char **directory_list = get_directory_contents(*(++argv), &dir_list_size);
	                    if (dir_list_size != 0)
	                    {
	                        int cmd = -1;
	                        while (cmd == -1)
	                        {
	                            printf("\nPlease select the configuration file :\n\n");
	                            int i;
	                            for (i = 0; i < dir_list_size; i++)
	                            {
	                                printf("%d-) %s \n", i + 1, *(directory_list + i));
	                            }
								cmd = getInteger(0, dir_list_size);
	                        }
	                        file_path = malloc(sizeof(char*) * strlen(*(directory_list + (cmd - 1))));
	                        strcpy(file_path, *(directory_list + (cmd - 1)));
	                    }
	                    else
	                    {
	                        printf("Selected folder is empty or not exists, using default configuration file.");
	                    }
                    }
                break;
                default:
                    printf("Unknown start parameter consider using;\n");
                    printf("-p <file_path for product info>\n");
                    printf("-f <file_path>\n");
                    printf("-d <directory_path>\n");
                break;
            }

            if (strchr(file_path, '\\') != NULL || strchr(file_path, '/') != NULL) // full path provided
            {
                file_name = strrchr(file_path, '\\');
                if (file_name == NULL)
                {
                    file_name = strrchr(file_path, '/');  // POSIX
                }
                file_name += 1; //Delete the first char
            }
            else
            {
                file_name = file_path;
            }
        }
        else
        {
            printf("Bad Syntax\n");
            printf("-p <file_path for product info>\n");
            printf("-f <file_path>\n");
            printf("-d <directory_path>\n");
        }
    }
	unlockMutex();
}

int main(int argc, char * argv[])
{
    PNIO_UINT32 ret_val = 0;
	int  cmd = 777;
    PNIO_BOOL infoFromTxt = PNIO_FALSE;
    PNIO_BOOL startup_done  = PNIO_FALSE;
	
	initMutex();
    
    process_start_args(argv, &infoFromTxt);
#if defined PLF_PNDRIVER_WIN
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif
	
    //After pnd_init you can use e.g. pntrc
	pnd_init_trace_levels(&debSet);

    while (cmd != 0)
    {
        lockMutex();
			
        printf("\n");
        HR(80);
        printf("PROFINET Driver | test program  \n");
        printf("================================\n");
        printf("1...Help (show menu)            \n");
        printf("================================\n");
        printf("2...SERV_CP_Startup             \n");
        printf("4...SERV_CP_Shutdown            \n");
        printf("================================\n");
        printf("5...Open Controller             \n");
        printf("6...Close Controller            \n");
        printf("================================\n");
        printf("7...Set Mode PNIO_MODE_OFFLINE  \n");
        printf("8...Set Mode PNIO_MODE_CLEAR    \n");
        printf("9...Set Mode PNIO_MODE_OPERATE  \n");
        printf("================================\n");
        printf("10...Device Activate            \n");
        printf("11...Device Deactivate          \n");
        printf("================================\n");
        printf("12...I&M Data Read              \n");
        printf("13...I&M Data Write             \n");
        printf("================================\n");
        printf("15...SERV_CP_set_trace_level    \n");
        printf("================================\n");
        printf("16...IO Data Read/Write         \n");
        printf("=============================== \n");
        printf("17...Perform record operations  \n");
        printf("================================\n");
        printf("18...List Modules and Submodules\n");
        printf("================================\n");
        printf("21...Open PDEV interface        \n");
        printf("22...Close PDEV interface       \n");
        printf("================================\n");
        printf("23...Interface Record Read      \n");
        printf("================================\n");
        printf("24...Interface Set IP and NoS   \n");
		printf("================================\n");
		printf("25...Enable/Disable SNMP        \n");
		printf("================================\n");
#if (defined PLF_PNDRIVER_LINUX_SOC1) && (defined PNDRIVER_USE_NATIVE_IPSTACK)
		printf("26...Enable/Disable SSH			\n");
        printf("================================\n");
#endif
		printf("27...Show PN Driver Version Info\n");
		printf("================================\n");
#if defined PNDTEST_USE_CIRCLE_TRACE
		printf("99...Write trace data to file   \n");
		printf("================================\n");
#endif
        printf("0... QUIT \n");
            
        unlockMutex();
			
		cmd = getInteger(0, 99);

		if (cmd != -1)
		{
			switch (cmd)
			{
				case 1:
				break; // Show menu
				case 2: // SERV_CP_Startup
				{
					if (pnd_test_startup(file_path, rema_file_name, infoFromTxt, product_file_path, preSelectedCp, &debSet) == PNIO_OK)
					{
						startup_done = PNIO_TRUE;
					}					
				}
				break;

				case 4: // SERV_CP_shutdown
				{
					if ( pnd_test_shutdown() == PNIO_OK ) 
					{
					    startup_done = PNIO_FALSE;				
					}	
				}
				break;

				case 5: // open controller
				{
					pnd_test_controller_open();
					pnd_test_register_setmode_cbf();
					pnd_test_register_devact_cbf();
					pnd_test_register_diag_req_cbf();
					pnd_test_register_iosystem_reconfig();
#if defined PNDRIVER_ISO_APP
					pnd_test_register_op_fault_ind_cbf();
					pnd_test_register_start_op_ind_cbf();
#endif
				}
				break;

				case 6: // close controller
				{
					pnd_test_controller_close();
				}
				break;

				case 7: // set mode offline
				{
					pnd_test_set_mode(PNIO_MODE_OFFLINE);
				}
				break;

				case 8: // set mode clear
				{
					pnd_test_set_mode(PNIO_MODE_CLEAR);
				}
				break;

				case 9: // set mode operate
				{
					pnd_test_set_mode(PNIO_MODE_OPERATE);
				}
				break;

				case 10: // device activate 
				{
					pnd_test_dev_activate(PNIO_DA_TRUE);
				}
				break;

				case 11: // device passivate 
				{
					pnd_test_dev_activate(PNIO_DA_FALSE);
				}
				break;

				case 12: // read I&M Data
				{
					pnd_test_IM_data_read();
				}
				break;

				case 13: // write I&M Data
				{
					pnd_test_IM_data_write();
				}
				break;

				case 15: // pntrc test
				{
					pnd_test_pntrc();
				}
				break;

				case 16: // PNIO_data_write() / PNIO_data_read()
				{
					pnd_test_iodu();
				}
				break;

				case 17:
				{
					pnd_test_record_update();
				}
				break;
				case 18:
				{
					pnd_test_list_devices();
				}
				break;
				case 21:
				{
					pnd_test_interface_open();
					pnd_test_register_interface_set_ip_and_nos();
					pnd_test_register_interface_rema_read();
				}
				break;
				case 22:
				{
					pnd_test_interface_close();
				}
				break;
				case 23:
				{
					pnd_test_interface_record_read();
				}
				break;
				case 24:
				{
					pnd_test_interface_set_ip_and_nos();
				}
				break;
				case 25:
				{
					pnd_test_enable_disable_snmp();
				}
				break;
#if (defined PLF_PNDRIVER_LINUX_SOC1) && (defined PNDRIVER_USE_NATIVE_IPSTACK)
				case 26:
				{
					pnd_test_enable_disable_ssh();
				}
				break;
#endif
				case 27:
				{
					pnd_test_display_version();
				}
				break;

#if defined PNDTEST_USE_CIRCLE_TRACE
				case 99:
				{
					if (startup_done == PNIO_TRUE)
					{
					    pnd_test_write_trace_buffer_to_filesystem(0);
					}
					else
					{
						printf("Please run startup with Menu Item 2 prior to dump circle trace \n");
					}						
				}
				break;
#endif
				case 0:
				{
					pnd_test_quit_application(g_ApplHandle);
				}
				break;

                default:
                {
                    printf("\nInvalid selection\n");
                }
                break;
			}
		}
    }
	
	release_trace_resources();

    if (strcmp(file_path, "PNDriverBase_TestApp/Station_1.PN Driver_1.PNDriverConfiguration.xml") != 0)
    {
        free(file_path);
    }

    if (strcmp(product_file_path, "") != 0)
    {
        free(product_file_path);
    }
    return (ret_val);

}  // end of main

/*****************************************************************************/
/*  Copyright (C) 2020 Siemens Aktiengesellschaft. All rights reserved.      */
/*****************************************************************************/
