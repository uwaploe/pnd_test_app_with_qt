# README #



### What is this repository for? ###

* A basic Qt test app to diagnose why the PN Driver library is throwing an assert error. This
  error is occuring in my real app, but I've created this test app as way to simplify the
  recreation of the problem. This app demonstrates that no profinet calls even need to be run
  to produce the assert error - just linking in libpndriver is enough.

### How do I get set up? ###

* Follow the Quick Start PROFINET Driver V2.2 section 3 to create a Debian 9.3 32-bit
  4.9.0-11-rt-686-pae machine, Linux variant (not Linux Native).
* Install Qt from the default debian repo:
* 'apt-get install qt5-default' may install all you need, if not then also install qttools5-dev-tools and qttools5-dev

### Creating the Makefile ###

* run 'qmake', which will read the test.pro file and generate a standard Makefile. 
* Running 'make' will build the app. 
* I've included a Makefile into the git repo, but run qmake to create a "fresh" Makefile. 
* Also re-run qmake if you make any changes to test.pro

### Important things to note ###

* No profinet calls are being made in the test app. SERV_CP_init() exists in an unused
 function, and is only to insure that the compiler links in libpndriver. If the compiler
 doesn't see any calls into a library it decides the library doesn't need to be included.

* If you comment out the single line in main.cpp containing SERV_CP_init() then this app will
  run without the pndriver library throwing an assert error (because it's not actually being
  included in the executable).

* Also note that there is nothing prohibitave about Qt or linking 3rd party libraries into Qt
  apps. I've used many 3rd party libraries in many Qt apps, including the pniouser library when
  using the CP1604 card

* The assert error appears to happen when the pndriver library runs its internal initialization
  code as the app is starting up. One question we've asked is are there defines or other
  variables that aren't getting set during compilation of this test app (and my real app)?

* The test_app in the pn_driver examples folder builds and runs without throwing an assert
  error, so the real question is: what is missing in this 'hello world' Qt test app that's causing
  the pndriver library to not properly initialize? If you compare the test.pro file (and the
  resulting Makefile) all the compiler flags appear to be the same as the Makefile included
  with the example test_app.