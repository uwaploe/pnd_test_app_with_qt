######################################################################
#  Qt project file. This file is read by qmake, which then produces
#  a standard Makefile
######################################################################
 

# Qt project file. The qmake tool turns this project file into a standard Makefile
# C/C++ build flags, defines, includes, library paths, etc are passed into the Makefile

# Tells qmake that it's building an application
TEMPLATE = app
QT +=  core gui widgets # standard Qt app with a gui displaying a widget

TARGET = test

SOURCE_ROOT_DIR_PNDRIVER = /usr/src/PNDriver_2.2.0/pn_driver/src/source/pnd/

# Path to external header files
INCLUDEPATH += \
./ \
$$SOURCE_ROOT_DIR_PNDRIVER/inc \   
$$SOURCE_ROOT_DIR_PNDRIVER/src/common \
/usr/src/PNDriver_2.2.0/pn_driver/src/examples/shared/src \
/usr/src/PNDriver_2.2.0/pn_driver/src/examples/test_app/src


# -D defines passed into the Makefile
DEFINES += PLF_PNDRIVER_LINUX TOOL_CHAIN_GNU NO_DEBUG

REMOVE_WARNINGS = -Wno-write-strings -Wno-unused-but-set-variable -Wno-type-limits -Wno-sign-compare -Wno-address -Wno-format 

QMAKE_CFLAGS +=          -c -m32 -pthread $$REMOVE_WARNINGS -O2 -Wall -fmessage-length=0 -MMD 
QMAKE_CXXFLAGS +=  -fPIC -c -m32 -pthread $$REMOVE_WARNINGS -O2 -Wall -fmessage-length=0 -MMD

QMAKE_LFLAGS += -m32 -pthread $(REMOVE_WARNINGS) -O2 -Wall -fmessage-length=0 -MD -MP
LIBS += -L/usr/src/PNDriver_2.2.0/pn_driver/src/examples/shared/linux32/build/lib
LIBS += -lpthread -lrt -ldl -lpndriver 

QMAKE_CLEAN += $$TARGET

# local headers and source files listed here
HEADERS += \


SOURCES += \
pnd_test.c \
/usr/src/PNDriver_2.2.0/pn_driver/src/examples/shared/src/pnd_trace_util.c \
/usr/src/PNDriver_2.2.0/pn_driver/src/examples/shared/src/pnd_iodu.c \
/usr/src/PNDriver_2.2.0/pn_driver/src/examples/shared/src/pnd_pntrc.c \
/usr/src/PNDriver_2.2.0/pn_driver/src/examples/shared/src/pnd_helper_functions.c \
/usr/src/PNDriver_2.2.0/pn_driver/src/examples/shared/src/pnd_iob_core.cpp \
/usr/src/PNDriver_2.2.0/pn_driver/src/examples/test_app/src/pnd_test_callback_functions.cpp
